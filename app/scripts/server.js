/*
 * Author: Yoann Bentz (Yoone)
 * Website: yoone.eu
 */

var EXT_ID = "ofoolnknledpecfbbfohnohbfhodeilh";

var config = {
    host: "0.0.0.0",
    tcpPort: 18984,
    udpPort: 18985
};

var nick = "No name";
chrome.runtime.getPlatformInfo(function(platform) {
    nick = platform.os.charAt(0).toUpperCase() + platform.os.slice(1);
});

/*
 * Binary helpers
 */

function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}

function str2ab(str) {
    var buf = new ArrayBuffer(str.length);
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

/*
 * Extension
 */

chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse) {
    if (typeof request.nick !== "undefined") {
        nick = request.nick;
    }
    else if (typeof request.client !== "undefined") {
        chrome.sockets.tcp.send(request.client, str2ab(JSON.stringify({tabs: request.infos}) + "\n"),
            function(code) {});
    }
});

/*
 * TCP Server (commands)
 */

chrome.sockets.tcpServer.create({}, function(info) {
    chrome.sockets.tcpServer.listen(info.socketId, config.host, config.tcpPort, function(result) {
        if (result < 0) {
            console.log("Error binding TCP socket");
            return;
        }
        console.log("Listening on " + config.host + ":" + config.tcpPort + " (TCP)");

        chrome.sockets.tcpServer.onAccept.addListener(function(info) {
            chrome.sockets.tcp.setPaused(info.clientSocketId, false);

            chrome.sockets.tcp.onReceive.addListener(function(result) {
                //console.log("Received: " + ab2str(result.data));
                var data = JSON.parse(ab2str(result.data));
                data.client = info.clientSocketId;
                chrome.runtime.sendMessage(EXT_ID, data, function(response) {});
            });
        });
    });
});

/*
 * UDP Server (receive broadcasts)
 */

chrome.sockets.udp.create({persistent:true}, function(socketInfo) {
    var socketId = socketInfo.socketId;

    chrome.sockets.udp.bind(socketId, config.host, config.udpPort, function(result) {
        if (result < 0) {
            console.log("Error binding UDP socket");
            return;
        }
        console.log("Listening on " + config.host + ":" + config.udpPort + " (UDP)");
    });

    chrome.sockets.udp.onReceive.addListener(function(info) {
        if (info.socketId !== socketId)
            return;
        chrome.sockets.udp.setPaused(socketId, true, function() {});

        console.log("Received broadcast packet from " + info.remoteAddress + ":" + info.remotePort);
        chrome.sockets.udp.send(info.socketId, str2ab(nick + "\n"), info.remoteAddress,
            info.remotePort, function(sendInfo) {});

        chrome.sockets.udp.setPaused(socketId, false, function() {});
    });
});
