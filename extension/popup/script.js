/*
 * Author: Yoann Bentz (Yoone)
 * Website: yoone.eu
 */

$(document).ready(function() {
    chrome.storage.local.get("nick", function(items) {
        if (typeof items.nick !== "undefined") {
            $("#nick").val(items.nick);
        }
    });
});

$("body").on("click", "#save", function() {
    var nick = $("#nick").val().trim();
    if (nick != "") {
        chrome.storage.local.set({nick:nick}, function() {
            $("#save").val("Saved!");
        });
    }
});
