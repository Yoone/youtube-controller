/*
 * Author: Yoann Bentz
 * Website: yoone.eu
 */

var VIDEO_TITLE = "h1.watch-title-container span";
var NEXT_BTN    = ".ytp-button-next";
var PREV_BTN    = ".ytp-button-prev";

function getVideo() {
    return document.getElementsByTagName("video")[0];
}

/*
 * Keep the volume preferences
 */

var userVolume = 1;

// When YouTube randomly resets the volume
getVideo().onvolumechange = function() {
    getVideo().volume = userVolume;
};

// When the video changes
setInterval(function() {
    getVideo().volume = userVolume;
}, 500);

/*
 * Player definition
 */

var player = {};

player.video = {
    playing: true,
    volume: 100,
    title: "",
    length: 0,
    current: 0,
    hasPrev: false
};

/*
 * Helper functions
 */

player.play = function() {
    getVideo().play();
};

player.pause = function() {
    getVideo().pause();
};

player.next = function() {
    $(NEXT_BTN).trigger("click");
};

player.prev = function() {
    $(PREV_BTN).trigger("click");
};

player.restart = function() {
    getVideo().currentTime = 0;
};

player.setVolume = function(value) {
    userVolume = parseInt(value) / 100;
    getVideo().volume = userVolume;
};

player.update = function() {
    player.video.title = $(VIDEO_TITLE).text().trim().replace(/\s+/g, " ");
    player.video.hasPrev = $(PREV_BTN).is(":visible");

    var video = getVideo();
    player.video.length = video.duration;
    player.video.current = video.currentTime;
    player.video.playing = !video.paused;
    player.video.volume = video.volume > 0.94 ? 100 : video.volume * 100; // Hack: YouTube max is 94%
};

/*
 * Event listener
 */

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request == "info") {
        player.update();
        sendResponse(player.video);
    }
    else if (request.command == "play")
        player.play();
    else if (request.command == "pause")
        player.pause();
    else if (request.command == "next")
        player.next();
    else if (request.command == "prev")
        player.prev();
    else if (request.command == "restart")
        player.restart();
    else if (request.command == "volume")
        player.setVolume(request.args[0]);
    else
        console.log("Error: unrecognized command");
});
