/*
 * Author: Yoann Bentz (Yoone)
 * Website: yoone.eu
 */

var APP_ID = "pldemfhfmpkceggbgafkmfamcpobckla";

/*
 * Tabs
 */

function visit_tab(infos, tabs, n, client) {
    if (n < tabs.length) {
        var tab = tabs[n];
        var parser = document.createElement("a");
        parser.href = tab.url;
        if (parser.hostname == "www.youtube.com" && parser.pathname == "/watch") {
            chrome.tabs.sendMessage(tab.id, "info", function(response) {
                if (response) {
                    response.tab = tab.id;
                    infos.push(response);
                }
                visit_tab(infos, tabs, n + 1, client);
            });
        }
        else
            visit_tab(infos, tabs, n + 1, client);
    }
    else {
        chrome.runtime.sendMessage(APP_ID, {client: client, infos: infos}, function(response) {});
    }
}

/*
 * App (server)
 */

setInterval(function() { // Hack: wake up app and send nick
    chrome.storage.local.get("nick", function(items) {
        chrome.runtime.sendMessage(APP_ID, items, function(response) {});
    });
}, 2000);

chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse) {
    if (request.tab) {
        chrome.tabs.sendMessage(request.tab, request, function(response) {});
    }
    else {
        var infos = [];
        chrome.tabs.query({}, function(tabs) {
            visit_tab(infos, tabs, 0, request.client);
        });
    }
});
