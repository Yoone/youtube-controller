package eu.yoone.youtube.util;

import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import eu.yoone.youtube.R;
import eu.yoone.youtube.activity.YoutubeActivity;
import eu.yoone.youtube.network.TcpClient;
import eu.yoone.youtube.view.ChromeTab;
import eu.yoone.youtube.view.ChromeTabAdapter;

/**
 * Created by yoone on 26/06/15.
 */
public class ChromeTabUtil {
    private static ChromeTabUtil instance;

    private YoutubeActivity activity;
    private ChromeTabAdapter adapter;
    private List<ChromeTab> tabs = new ArrayList<>();

    private ListView layoutTabs;
    private ProgressBar layoutLoading;

    private Integer selectedTab;
    private Integer selectedTabPosition;

    public Integer getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(Integer selectedTab) {
        this.selectedTab = selectedTab;
    }

    public Integer getSelectedTabPosition() {
        return selectedTabPosition;
    }

    public void setSelectedTabPosition(Integer selectedTabPosition) {
        this.selectedTabPosition = selectedTabPosition;
    }

    private ChromeTabUtil() {
    }

    public void set(YoutubeActivity activity, ChromeTabAdapter adapter) {
        this.activity = activity;
        this.adapter = adapter;
        layoutTabs = (ListView) activity.findViewById(R.id.listTabs);
        layoutLoading = (ProgressBar) activity.findViewById(R.id.loadingBar);
    }

    public static ChromeTabUtil get() {
        if (instance == null) {
            instance = new ChromeTabUtil();
        }
        return instance;
    }

    public List<ChromeTab> getTabs() {
        return tabs;
    }

    public void incrementProgress() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (ChromeTab tab : tabs) {
                    if (tab.getPlaying()) {
                        if (tab.getCurrent() < tab.getLength()) {
                            tab.incrementCurrent();
                        }
                        else {
                            tab.setPlaying(false);
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void setTabs(final List<ChromeTab> newTabs) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tabs.clear();
                for (ChromeTab tab : newTabs) {
                    tabs.add(tab);
                }
                adapter.notifyDataSetChanged();
                activity.setMessageVisibility(tabs.size() == 0 ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    public void layoutClientConnection() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (TcpClient.get().isConnected() && layoutTabs.getVisibility() == View.INVISIBLE) {
                    layoutLoading.setVisibility(View.INVISIBLE);
                    layoutTabs.setVisibility(View.VISIBLE);
                }
                else if (!TcpClient.get().isConnected() && layoutLoading.getVisibility() == View.INVISIBLE) {
                    layoutLoading.setVisibility(View.VISIBLE);
                    layoutTabs.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
