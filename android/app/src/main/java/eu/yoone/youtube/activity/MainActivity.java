package eu.yoone.youtube.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import eu.yoone.youtube.R;
import eu.yoone.youtube.view.ServerInfo;
import eu.yoone.youtube.network.UdpBroadcast;
import eu.yoone.youtube.network.UdpTask;
import eu.yoone.youtube.util.ServerUtil;
import eu.yoone.youtube.view.ServerInfoAdapter;

/**
 * Created by yoone on 26/06/15.
 */
public class MainActivity extends ActionBarActivity implements UdpTask {
    private UdpBroadcast udpBroadcast;
    private ServerInfoAdapter serverInfoAdapter;
    private SwipeRefreshLayout refreshLayout;
    private ListView serverView;
    private TextView noServerMessage;

    private class ServerClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Intent intent = new Intent(MainActivity.this, YoutubeActivity.class);
            ServerInfo server = ServerUtil.get().get(position);
            intent.putExtra("server", server);
            intent.putExtra("ParentClassName", "MainActivity");
            startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(R.string.activity_main);

        serverView = (ListView) findViewById(R.id.listServers);
        noServerMessage = (TextView) findViewById(R.id.noServerMessage);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshServersContainer);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MainActivity.this.noServerMessage.setVisibility(View.INVISIBLE);
                MainActivity.this.udpBroadcast = new UdpBroadcast(MainActivity.this);
                MainActivity.this.udpBroadcast.execute();
            }
        });

        serverInfoAdapter = new ServerInfoAdapter(this);
        serverView.setAdapter(serverInfoAdapter);
        serverView.setOnItemClickListener(new ServerClickListener());
        serverView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (serverView == null || serverView.getChildCount() == 0) ? 0 : serverView.getChildAt(0).getTop();
                MainActivity.this.refreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0 || visibleItemCount == totalItemCount);
            }
        });

        refreshLayout.post(new Runnable() { // Trigger the swiping animation
            @Override
            public void run() {
                MainActivity.this.refreshLayout.setRefreshing(true);
            }
        });
        udpBroadcast = new UdpBroadcast(this);
        udpBroadcast.execute();
    }

    @Override
    public void onTaskCompleted() {
        List<ServerInfo> infos = null;
        try {
            infos = udpBroadcast.get();
        } catch (Exception e) {
            Toast.makeText(this, "Error: could not retrieve server list", Toast.LENGTH_SHORT).show();
        } finally {
            if (infos == null) {
                Toast.makeText(this, "Error: could not retrieve server list", Toast.LENGTH_SHORT).show();
            }
            else {
                ServerUtil.get().clear();
                for (ServerInfo info : infos) {
                    ServerUtil.get().add(info);
                }
                serverInfoAdapter.notifyDataSetChanged();
            }
            refreshLayout.setRefreshing(false);
            if (ServerUtil.get().size() == 0) {
                noServerMessage.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
