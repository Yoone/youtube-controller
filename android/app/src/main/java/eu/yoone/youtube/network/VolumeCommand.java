package eu.yoone.youtube.network;

import java.util.ArrayList;
import java.util.List;

import eu.yoone.youtube.view.ChromeTab;

/**
 * Created by yoone on 26/06/15.
 */
public class VolumeCommand extends Command {
    private Integer volume;

    public VolumeCommand(ChromeTab tab, Integer volume) {
        super(tab);
        this.volume = volume;
    }

    @Override
    protected List<String> getArgs() {
        List<String> list = new ArrayList<>();
        list.add(volume.toString());
        return list;
    }

    @Override
    protected String getCommand() {
        return "volume";
    }
}
