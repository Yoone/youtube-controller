package eu.yoone.youtube.network;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import eu.yoone.youtube.view.ServerInfo;

/**
 * Created by yoone on 27/06/15.
 */
public class UdpBroadcast extends AsyncTask<Integer, Integer, List<ServerInfo>> {
    public static final int BROADCAST_PORT = 18985;
    public static final int BROADCAST_TIMEOUT = 5000;

    private UdpTask callback;
    private List<ServerInfo> infos;
    private DatagramSocket socket;

    public UdpBroadcast(UdpTask callback) {
        this.callback = callback;
        this.infos = new ArrayList<>();
    }

    @Override
    protected List<ServerInfo> doInBackground(Integer... params) {
        try {
            String message = "ytc";

            socket = new DatagramSocket();
            socket.setBroadcast(true);
            socket.setSoTimeout(BROADCAST_TIMEOUT);

            InetAddress address = InetAddress.getByName("255.255.255.255");
            DatagramPacket packet = new DatagramPacket(message.getBytes(), message.length(), address, BROADCAST_PORT);
            socket.send(packet);

            byte[] buffer = new byte[1024];
            long start = System.currentTimeMillis();
            while (System.currentTimeMillis() - start < BROADCAST_TIMEOUT) {
                DatagramPacket result = new DatagramPacket(buffer, buffer.length);
                socket.receive(result);

                String serverOs = new Scanner(new String(result.getData())).nextLine();
                String serverIp = result.getAddress().toString().substring(1);
                infos.add(new ServerInfo(serverOs, serverIp));
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            // Do nothing, because this is very likely to happen with the loop receiving the packets
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace(); // TODO: find a proper way to handle *ALL* exceptions in the app
        }
        finally {
            socket.close();
            return infos;
        }
    }

    @Override
    protected void onPostExecute(List<ServerInfo> infos) {
        callback.onTaskCompleted();
    }
}
