package eu.yoone.youtube.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import eu.yoone.youtube.network.Command;

/**
 * Created by yoone on 26/06/15.
 */
public class ClientCommandsUtil {
    private static List<Command> commands;

    private ClientCommandsUtil() {
    }

    public static List<Command> get() {
        if (commands == null) {
            commands = new CopyOnWriteArrayList<>();
        }
        return commands;
    }
}
