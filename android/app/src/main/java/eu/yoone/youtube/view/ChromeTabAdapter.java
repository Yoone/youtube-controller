package eu.yoone.youtube.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import eu.yoone.youtube.R;
import eu.yoone.youtube.network.Command;
import eu.yoone.youtube.network.NextCommand;
import eu.yoone.youtube.network.PauseCommand;
import eu.yoone.youtube.network.PlayCommand;
import eu.yoone.youtube.network.PrevCommand;
import eu.yoone.youtube.network.RefreshCommand;
import eu.yoone.youtube.network.RestartCommand;
import eu.yoone.youtube.network.VolumeCommand;
import eu.yoone.youtube.util.ChromeTabUtil;
import eu.yoone.youtube.util.ClientCommandsUtil;

/**
 * Created by yoone on 26/06/15.
 */
public class ChromeTabAdapter extends ArrayAdapter<ChromeTab> {
    private class PlayButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Integer position = ChromeTabUtil.get().getSelectedTabPosition();
            if (position == null) {
                return;
            }
            ChromeTab tab = ChromeTabUtil.get().getTabs().get(position);
            Boolean newState = !tab.getPlaying();
            if (tab.getPlaying()) {
                ClientCommandsUtil.get().add(new PauseCommand(tab));
            }
            else {
                ClientCommandsUtil.get().add(new PlayCommand(tab));
            }
            // Not using !tab.getPlaying() because the Pause/Play command can update the
            // playing state before the line below is reached
            tab.setPlaying(newState);
            if (tab.getCurrent().equals(tab.getLength())) {
                tab.setCurrent(0);
            }
            ChromeTabAdapter.this.notifyDataSetChanged();
        }
    }

    private class NavigationButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Integer position = ChromeTabUtil.get().getSelectedTabPosition();
            if (position == null) {
                return;
            }
            ChromeTab tab = ChromeTabUtil.get().getTabs().get(position);
            // Next and Prev buttons: pausing the video for the user to know the
            // video is changing when the button is pressed
            switch (v.getId()) {
                case R.id.nextButton:
                    ClientCommandsUtil.get().add(new PauseCommand(tab));
                    ClientCommandsUtil.get().add(new NextCommand(tab));
                    break;
                case R.id.prevButton:
                    ClientCommandsUtil.get().add(new PauseCommand(tab));
                    ClientCommandsUtil.get().add(new PrevCommand(tab));
                    break;
                case R.id.restartButton:
                    ClientCommandsUtil.get().add(new RestartCommand(tab));
                    break;
                default:
                    return;
            }
            ChromeTabAdapter.this.notifyDataSetChanged();
        }
    }

    private class VolumeButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            final Integer position = ChromeTabUtil.get().getSelectedTabPosition();
            if (position == null) {
                return;
            }
            final ChromeTab tab = ChromeTabUtil.get().getTabs().get(position);

            View layout = LayoutInflater.from(getContext()).inflate(R.layout.volume_dialog, null);
            final SeekBar volume = (SeekBar) layout.findViewById(R.id.volumeSeekBar);
            volume.setProgress(tab.getVolume());

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setView(layout);
            builder.setMessage(tab.getTitle());
            builder.setPositiveButton(R.string.dialog_volume_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ClientCommandsUtil.get().add(new VolumeCommand(tab, volume.getProgress()));
                    ChromeTab tab = ChromeTabUtil.get().getTabs().get(position);
                    dialog.dismiss();
                    ChromeTabAdapter.this.notifyDataSetChanged();
                }
            });
            builder.setNegativeButton(R.string.dialog_volume_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.setTitle(getContext().getResources().getString(R.string.dialog_volume_title));
            dialog.show();
        }
    }

    private static class TabHolder {
        public TextView title;
        public ProgressBar progress;
        public ImageView play;
        public TextView current;
        public TextView length;
        public LinearLayout tabMenu;
        public Button playButton;
        public Button nextButton;
        public Button prevButton;
        public Button restartButton;
        public Button volumeButton;
    }

    private Context context;

    public ChromeTabAdapter(Context context) {
        super(context, R.layout.list_tab, ChromeTabUtil.get().getTabs());
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;

        TabHolder holder = new TabHolder();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.list_tab, parent, false);

            row.setTag(holder);
        }
        else {
            row = convertView;
        }

        holder.title = (TextView) row.findViewById(R.id.videoTitle);
        holder.progress = (ProgressBar) row.findViewById(R.id.videoProgress);
        holder.play = (ImageView) row.findViewById(R.id.playStatus);
        holder.current = (TextView) row.findViewById(R.id.videoCurrent);
        holder.length = (TextView) row.findViewById(R.id.videoLength);
        holder.tabMenu = (LinearLayout) row.findViewById(R.id.tabMenu);
        holder.playButton = (Button) row.findViewById(R.id.playButton);
        holder.nextButton = (Button) row.findViewById(R.id.nextButton);
        holder.prevButton = (Button) row.findViewById(R.id.prevButton);
        holder.restartButton = (Button) row.findViewById(R.id.restartButton);
        holder.volumeButton = (Button) row.findViewById(R.id.volumeButton);

        ChromeTab tab = ChromeTabUtil.get().getTabs().get(position);

        if (tab != null) {
            holder.title.setText(tab.getTitle());
            holder.progress.setProgress(tab.getCurrent());
            holder.progress.setMax(tab.getLength());
            holder.current.setText(tab.getFormattedCurrent());
            holder.length.setText(tab.getFormattedLength());

            int drawable;
            int strPlay;
            if (tab.getPlaying()) {
                drawable = R.drawable.playback_play;
                strPlay = R.string.button_pause;
            }
            else {
                drawable = R.drawable.playback_stop;
                strPlay = R.string.button_play;
            }
            Drawable button = getContext().getResources().getDrawable(drawable);
            holder.play.setImageDrawable(button);
            holder.playButton.setText(row.getResources().getString(strPlay));

            if (tab.getId().equals(ChromeTabUtil.get().getSelectedTab())) {
                holder.tabMenu.setVisibility(View.VISIBLE);
                holder.playButton.setOnClickListener(new PlayButtonClickListener());
                holder.nextButton.setOnClickListener(new NavigationButtonClickListener());
                holder.prevButton.setOnClickListener(new NavigationButtonClickListener());
                holder.prevButton.setEnabled(tab.hasPrev());
                holder.restartButton.setOnClickListener(new NavigationButtonClickListener());
                holder.volumeButton.setText(String.format("%d%%", tab.getVolume()));
                holder.volumeButton.setOnClickListener(new VolumeButtonClickListener());
            }
            else {
                holder.tabMenu.setVisibility(View.GONE);
            }
        }
        return row;
    }
}
