package eu.yoone.youtube.util;

import java.util.ArrayList;
import java.util.List;

import eu.yoone.youtube.view.ServerInfo;

/**
 * Created by yoone on 27/06/15.
 */
public class ServerUtil {
    private static List<ServerInfo> servers;

    private ServerUtil() {
    }

    public static List<ServerInfo> get() {
        if (servers == null) {
            servers = new ArrayList<>();
        }
        return servers;
    }
}
