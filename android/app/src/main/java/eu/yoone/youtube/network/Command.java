package eu.yoone.youtube.network;

import java.util.ArrayList;
import java.util.List;

import eu.yoone.youtube.view.ChromeTab;

/**
 * Created by yoone on 26/06/15.
 */
public abstract class Command {
    private ChromeTab tab;

    public Command() {
    }

    public Command(ChromeTab tab) {
        this.tab = tab;
    }

    protected abstract String getCommand();

    protected List<String> getArgs() {
        return new ArrayList<>();
    }

    public String getJSON() {
        String result = "{\"tab\":" + tab.getId() + ",\"command\":\"" + getCommand() + "\"";
        List<String> args = getArgs();
        if (args.size() > 0) {
            result += ",\"args\":[";
            for (int i = 0; i < args.size(); ++i) {
                result += "\"" + args.get(i) + "\"";
                if (i + 1 < args.size()) {
                    result += ",";
                }
            }
            result += "]";
        }
        result += "}\n";
        return result;
    }
}
