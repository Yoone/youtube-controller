package eu.yoone.youtube.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import eu.yoone.youtube.R;
import eu.yoone.youtube.network.PauseCommand;
import eu.yoone.youtube.network.PlayCommand;
import eu.yoone.youtube.network.TcpClient;
import eu.yoone.youtube.view.ChromeTab;
import eu.yoone.youtube.view.ChromeTabAdapter;
import eu.yoone.youtube.util.ChromeTabUtil;
import eu.yoone.youtube.util.ClientCommandsUtil;
import eu.yoone.youtube.view.ServerInfo;

/**
 * Created by yoone on 26/06/15.
 */
public class YoutubeActivity extends ActionBarActivity {
    public ChromeTabAdapter tabAdapter;
    private TextView noTabMessage;

    private class TabClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            ChromeTab tab = ChromeTabUtil.get().getTabs().get(position);
            if (tab.getId().equals(ChromeTabUtil.get().getSelectedTab())) {
                ChromeTabUtil.get().setSelectedTab(null);
            }
            else {
                ChromeTabUtil.get().setSelectedTab(tab.getId());
                ChromeTabUtil.get().setSelectedTabPosition(position);
            }
            tabAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);

        tabAdapter = new ChromeTabAdapter(this);

        noTabMessage = (TextView) findViewById(R.id.noTabMessage);
        ListView tabView = (ListView) findViewById(R.id.listTabs);
        tabView.setAdapter(tabAdapter);
        tabView.setOnItemClickListener(new TabClickListener());

        ChromeTabUtil.get().set(this, tabAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();

        ServerInfo server = getIntent().getParcelableExtra("server");
        TcpClient.get().setServer(server);
        Thread thClient = new Thread(TcpClient.get());
        thClient.start();
    }

    @Override
    public void onStop() {
        TcpClient.get().disconnect();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                TcpClient.get().stopTimers();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        TcpClient.get().stopTimers();
        super.onBackPressed();
    }

    public void setMessageVisibility(int visibility) {
        noTabMessage.setVisibility(visibility);
    }
}
