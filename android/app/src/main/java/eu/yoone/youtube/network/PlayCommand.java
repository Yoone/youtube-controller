package eu.yoone.youtube.network;

import eu.yoone.youtube.view.ChromeTab;

/**
 * Created by yoone on 26/06/15.
 */
public class PlayCommand extends Command {
    public PlayCommand(ChromeTab tab) {
        super(tab);
    }

    @Override
    protected String getCommand() {
        return "play";
    }
}
