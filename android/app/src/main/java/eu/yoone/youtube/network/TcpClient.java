package eu.yoone.youtube.network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import eu.yoone.youtube.view.ChromeTab;
import eu.yoone.youtube.util.ChromeTabUtil;
import eu.yoone.youtube.util.ClientCommandsUtil;
import eu.yoone.youtube.view.ServerInfo;

/**
 * Created by yoone on 26/06/15.
 */
public class TcpClient implements Runnable {
    private static TcpClient instance;

    public static final int SERVER_PORT = 18984;
    private ServerInfo server;
    private ScheduledExecutorService scheduledExec;

    private BufferedReader sockIn;
    private PrintWriter sockOut;

    private Boolean connected = false;
    private Boolean forceDisconnect = false;

    private TcpClient() {
    }

    public static TcpClient get() {
        if (instance == null) {
            instance = new TcpClient();
        }
        return instance;
    }

    public void setServer(ServerInfo server) {
        this.server = server;
    }

    public Boolean isConnected() {
        return connected;
    }

    public void disconnect() {
        stopTimers();
        forceDisconnect = true;
    }

    public void stopTimers() {
        if (scheduledExec != null) {
            scheduledExec.shutdown();
        }
        scheduledExec = Executors.newSingleThreadScheduledExecutor();
        ClientCommandsUtil.get().clear();
    }

    @Override
    public void run() {
        stopTimers();

        scheduledExec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                ChromeTabUtil.get().incrementProgress();
                if (TcpClient.this.isConnected()) {
                    for (Command command : ClientCommandsUtil.get()) {
                        if (command.getClass() == RefreshCommand.class) {
                            return;
                        }
                    }
                    ClientCommandsUtil.get().add(new RefreshCommand());
                }
            }
        }, 0, 1, TimeUnit.SECONDS);

        forceDisconnect = false;
        while (!forceDisconnect) {
            perform();
            ChromeTabUtil.get().layoutClientConnection();
            // TODO: timeout after some time (say 60 seconds)
        }
    }

    private void perform() {
        Socket socket = null;
        try {
            InetAddress serverAddr = InetAddress.getByName(server.getIp());
            socket = new Socket(serverAddr, SERVER_PORT);

            sockIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            sockOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

            connected = true;
            ChromeTabUtil.get().layoutClientConnection();
            ClientCommandsUtil.get().add(new RefreshCommand());

            Long lastUpdate = System.currentTimeMillis();
            while (socket.isConnected() && !forceDisconnect) {
                if (lastUpdate + 10000 < System.currentTimeMillis()) {
                    break;
                }

                if (sockIn.ready()) {
                    lastUpdate = System.currentTimeMillis();
                    final String received = sockIn.readLine();
                    if (received != null) {
                        JSONObject json;
                        try {
                            json = new JSONObject(received);
                            addTabs(json);
                        } catch (JSONException e) {
                            e.printStackTrace(); // JSONObject instanciation
                        }
                    }
                }

                Iterator<Command> iter = ClientCommandsUtil.get().iterator();
                Boolean alreadyRefreshed = false;
                while (iter.hasNext()) {
                    Command cmd = iter.next();

                    if (cmd.getClass() == RefreshCommand.class) {
                        // Beware! Dirty fix
                        // Avoid multiple refreshments in case of concurrent access
                        // (see iter.remove() below)
                        if (!alreadyRefreshed) {
                            sendData(cmd.getJSON());
                            alreadyRefreshed = true;
                        }
                    } else {
                        sendData(cmd.getJSON());
                    }

                    try {
                        ClientCommandsUtil.get().remove(cmd);
                    }
                    catch (ConcurrentModificationException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace(); // InetAddress.getByName
        } catch (IOException e) {
            e.printStackTrace(); // Socket constructor
        } finally {
            connected = false;
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (Exception e) {
                // There is nothing to do!
            }
        }
    }

    private void addTabs(JSONObject json) {
        List<ChromeTab> tabList = new ArrayList<>();
        try {
            JSONArray tabs = (JSONArray) json.get("tabs");
            for (int i = 0; i < tabs.length(); ++i) {
                JSONObject tab = (JSONObject) tabs.get(i);
                Boolean playing = tab.getBoolean("playing");
                Integer volume = tab.getInt("volume");
                String title = tab.getString("title");
                Integer length = tab.getInt("length");
                Integer current = tab.getInt("current");
                Boolean hasPrev = tab.getBoolean("hasPrev");
                Integer tabId = tab.getInt("tab");
                tabList.add(new ChromeTab(tabId, playing, volume, title, length, current, hasPrev));
            }
            ChromeTabUtil.get().setTabs(tabList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendData(String data) {
        sockOut.println(data);
        sockOut.flush();
    }
}
