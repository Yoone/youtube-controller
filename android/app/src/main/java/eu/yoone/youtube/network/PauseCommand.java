package eu.yoone.youtube.network;

import eu.yoone.youtube.view.ChromeTab;

/**
 * Created by yoone on 26/06/15.
 */
public class PauseCommand extends Command {
    public PauseCommand(ChromeTab tab) {
        super(tab);
    }

    @Override
    protected String getCommand() {
        return "pause";
    }
}
