package eu.yoone.youtube.network;

import eu.yoone.youtube.view.ChromeTab;

/**
 * Created by yoone on 26/06/15.
 */
public class RestartCommand extends Command {
    public RestartCommand(ChromeTab tab) {
        super(tab);
    }

    @Override
    protected String getCommand() {
        return "restart";
    }
}
