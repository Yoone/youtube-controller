package eu.yoone.youtube.view;

/**
 * Created by yoone on 26/06/15.
 */
public class ChromeTab {
    private Integer id;
    private Boolean playing;
    private Integer volume;
    private String title;
    private Integer length;
    private Integer current;
    private Boolean hasPrev;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getPlaying() {
        return playing;
    }

    public void setPlaying(Boolean playing) {
        this.playing = playing;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getLength() {
        return length;
    }

    public String getFormattedLength() {
        return String.format("%d:%02d", length / 60, length % 60);
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getCurrent() {
        return current;
    }

    public String getFormattedCurrent() {
        return String.format("%d:%02d", current / 60, current % 60);
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public void incrementCurrent() {
        ++current;
    }

    public Boolean hasPrev() {
        return hasPrev;
    }

    public void setHasPrev(Boolean hasPrev) {
        this.hasPrev = hasPrev;
    }

    public ChromeTab(Integer id, Boolean playing, Integer volume, String title, Integer length, Integer current, Boolean hasPrev) {
        this.id = id;
        this.playing = playing;
        this.volume = volume;
        this.title = title;
        this.length = length;
        this.current = current;
        this.hasPrev = hasPrev;
    }
}
