package eu.yoone.youtube.view;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yoone on 27/06/15.
 */
public class ServerInfo implements Parcelable {
    private String name;
    private String ip;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ServerInfo(String name, String ip) {
        this.name = name;
        this.ip = ip;
    }

    private ServerInfo(Parcel in) {
        name = in.readString();
        ip = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(ip);
    }

    public static final Parcelable.Creator<ServerInfo> CREATOR = new Parcelable.Creator<ServerInfo>() {
        @Override
        public ServerInfo createFromParcel(Parcel source) {
            return new ServerInfo(source);
        }

        @Override
        public ServerInfo[] newArray(int size) {
            return new ServerInfo[size];
        }
    };
}
