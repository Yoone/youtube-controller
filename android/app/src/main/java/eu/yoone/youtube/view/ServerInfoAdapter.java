package eu.yoone.youtube.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eu.yoone.youtube.R;
import eu.yoone.youtube.util.ChromeTabUtil;
import eu.yoone.youtube.util.ServerUtil;

/**
 * Created by yoone on 26/06/15.
 */
public class ServerInfoAdapter extends ArrayAdapter<ServerInfo> {
    private static class ServerHolder {
        public TextView ip;
        public TextView name;
    }

    private Context context;

    public ServerInfoAdapter(Context context) {
        super(context, R.layout.list_server, ServerUtil.get());
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;

        ServerHolder holder = new ServerHolder();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.list_server, parent, false);

            row.setTag(holder);
        }
        else {
            row = convertView;
        }

        holder.ip = (TextView) row.findViewById(R.id.serverIp);
        holder.name = (TextView) row.findViewById(R.id.serverName);

        ServerInfo info = ServerUtil.get().get(position);

        if (info != null) {
            holder.ip.setText(info.getIp());
            holder.name.setText(info.getName());
        }
        return row;
    }
}
